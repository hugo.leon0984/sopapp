﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sopinfra.Models
{
    public class App
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contacto { get; set; }
        public string Descripcion { get; set; }
        public string Equipo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime? LastUpdateDateTime { get; set; }
    }
}