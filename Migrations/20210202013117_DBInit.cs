﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace sopinfra.Migrations
{
    public partial class DBInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Apps",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    Contacto = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    Descripcion = table.Column<string>(type: "nvarchar(250)", nullable: false),
                    Equipo = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastUpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    Aplicacion = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    Descripcion = table.Column<string>(type: "nvarchar(250)", nullable: false),
                    Url = table.Column<string>(type: "nvarchar(350)", nullable: false),
                    FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastUpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Document", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Servers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DnsName = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    IpAddress = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    Descripcion = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    Sistema = table.Column<string>(type: "nvarchar(15)", nullable: false),
                    FechaRegistro = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastUpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Server", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "Idx_Descripcion",
                table: "Apps",
                column: "Descripcion");

            migrationBuilder.CreateIndex(
                name: "Idx_DnsName",
                table: "Apps",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "Idx_IpAddress",
                table: "Apps",
                column: "Contacto");

            migrationBuilder.CreateIndex(
                name: "Idx_Sistema",
                table: "Apps",
                column: "Equipo");

            migrationBuilder.CreateIndex(
                name: "Idx_Descripcion",
                table: "Documents",
                column: "Descripcion");

            migrationBuilder.CreateIndex(
                name: "Idx_DnsName",
                table: "Documents",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "Idx_IpAddress",
                table: "Documents",
                column: "Aplicacion");

            migrationBuilder.CreateIndex(
                name: "Idx_Sistema",
                table: "Documents",
                column: "Url");

            migrationBuilder.CreateIndex(
                name: "Idx_Descripcion",
                table: "Servers",
                column: "Descripcion");

            migrationBuilder.CreateIndex(
                name: "Idx_DnsName",
                table: "Servers",
                column: "DnsName");

            migrationBuilder.CreateIndex(
                name: "Idx_IpAddress",
                table: "Servers",
                column: "IpAddress");

            migrationBuilder.CreateIndex(
                name: "Idx_Sistema",
                table: "Servers",
                column: "Sistema");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Apps");

            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "Servers");
        }
    }
}
