﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Text.RegularExpressions;


namespace sopinfra.Models
{
    public class Servicios
    {
       
        public void Runstopservice(string id, string servicio)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
            "powershell ; " +
            "\"Stop-Service -Name \"" + servicio);
            string input = ps.Result;
            client.Disconnect();
        }


        public void Runstartservice(string id, string servicio)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
            "powershell ; " +
            "\"Start-Service -Name \"" + servicio);
            string input = ps.Result;
            client.Disconnect();
        }


        public string Runstatusservice(string id, string servicio)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
            "powershell ; " +
            "\"Get-Service -Name " + servicio + "  | Format-Table -AutoSize -HideTableHeaders \";");
            string input = ps.Result;
            client.Disconnect();
            return input;



        }

        public (string, List<string>) Runservicios(string id)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();

            var ps1 = client.RunCommand(
            "powershell.exe ; " +
            "\"Get-Service | Format-Table -AutoSize -Property Status,Name,DisplayName\";");

            var ps2 = client.RunCommand(
            "powershell.exe ; " +
            "\"Get-Service | select -Property Name\"; ");
        

            string input = ps2.Result;
            List<string> resB = new List<string>(
                   input.Split(new string[] { "\r\n" },
                   StringSplitOptions.RemoveEmptyEntries)).ToList();


            string resA = ps1.Result;
            //string resB = ps2.Result;
            
            

            



            client.Disconnect();
            return (resA, resB);



        }


    }
}
