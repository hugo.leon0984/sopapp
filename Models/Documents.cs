﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sopinfra.Models
{
    public class Document
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Aplicacion { get; set; }
        public string Descripcion { get; set; }
        public string Url { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime? LastUpdateDateTime { get; set; }
    }
}