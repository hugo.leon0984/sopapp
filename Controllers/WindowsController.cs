﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.ObjectModel;
using System.Diagnostics;
using sopinfra.Models;
using sopinfra.DBContexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;


namespace sopinfra.Controllers
{


    public class WindowsController : Controller
    {
        readonly WindowsCheck check = new WindowsCheck();
        readonly Servicios service = new Servicios();
        readonly Sitios site = new Sitios();

        private readonly IConfiguration configuration;

   //     public WindowsController(IConfiguration iConfig)
   //     {
   //         configuration = iConfig;
   //     }


        private readonly MyDBContext myDbContext;
        public WindowsController(MyDBContext context, IConfiguration iConfig)
        {
            myDbContext = context;
            configuration = iConfig;
        }


        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult InicioWin()
        {
            ViewBag.Servers = myDbContext.Servers.Where(x => x.Sistema == "Windows").ToList();
            return View();
        }

        public IActionResult Servicios(string id, string servicio, string opc_service)
        {

            var server = id;
            var result = service.Runservicios(id);


            ViewData["servicio"] = result.Item1;
            // ViewData["servicename"] = result.Item2;
          //  ViewData["cpu"] = result.Item3;
          //  ViewData["shared"] = result.Item4;
          //  ViewData["iis"] = result.Item8;
            ViewData["data"] = server;
            List<string> status = result.Item2;
          //  List<string> status = service.Runserviceprocess(server);
            //ViewBag.Status = result.Item2;

            // var prueba = result.Item2;


            // List<string> hola = prueba;


            //      ViewData["servicio"] = comando.Statusservicio(server);
            //      ViewData["cpu"] = comando.Statuscpu(server);
            //      ViewData["shared"] = comando.Statusshared(server);
            //      ViewData["iis"] = comando.Runstatusweb(server);
            //      ViewData["data"] = server;
            //      List<string> hola = comando.Runserviceprocess(server);
            // milinux.statuscpu(server);
            // ViewData["cpu"] = ;

            if (opc_service == "Start")
            {
                service.Runstartservice(server, servicio);
                ViewData["status"] = service.Runstatusservice(server, servicio);

                return View(status);
            }

            if (opc_service == "Stop")
            {
                service.Runstopservice(server, servicio);
                ViewData["status"] = service.Runstatusservice(server, servicio);

                return View(status);
            }
            if (opc_service == "Status")
            {
                // List<string> respuesta = milinux.runtestprocess(server);
                ViewData["status"] = service.Runstatusservice(server, servicio);
                //List<string> status = milinux.runstatusprocess(server,servicio);

                return View(status);
            }

            return View(status);
        }



        public IActionResult Inicio(string id)
        {
            var server = id;
            string user = configuration.GetSection("UserConect").GetSection("usuario").Value;
            ViewData["data"] = server;
            var result = check.Runserver(id,user);
            ViewData["servicio"] = result.Item1;
            // ViewData["servicename"] = result.Item2;
            
            ViewData["cpu"] = result.Item3;
            ViewData["shared"] = result.Item4;
            ViewData["iis"] = result.Item2;
           // ViewData["data"] = server;
    //        if (result.Item8 == "")
    //        {
    //            var test = "probando";
    //           ViewData["probando"] = test;
    //        }
            //    ViewBag.Servers = myDbContext.Servers.ToList();
            //ViewBag.Servers = ctx.Servers.Where(x=>x.Sistema == "Windows").ToList();
            //return RedirectToAction("Home", "Index");
            //    return View();
            return View();
        }
        public IActionResult Sitios(string id)
        {
            string user = configuration.GetSection("UserConect").GetSection("usuario").Value;
            string key = configuration.GetSection("UserConect").GetSection("privatekey").Value;
            var server = id;
            
            ViewData["data"] = server;
            //var result = site.Runserversites(id);
            //ViewData["iis"] = result;
            ViewData["iis"] = site.Runserversites(id,user,key);

            //    ViewBag.Servers = myDbContext.Servers.ToList();
            //ViewBag.Servers = ctx.Servers.Where(x=>x.Sistema == "Windows").ToList();
            //return RedirectToAction("Home", "Index");
            //    return View();
            return View();
        }
        public IActionResult Cpu(string id)
        {
            string user = configuration.GetSection("UserConect").GetSection("usuario").Value;
            var server = id;
            ViewData["data"] = server;
            var result = check.Runserver(id, user);
            ViewData["cpu"] = result.Item3;

            //    ViewBag.Servers = myDbContext.Servers.ToList();
            //ViewBag.Servers = ctx.Servers.Where(x=>x.Sistema == "Windows").ToList();
            //return RedirectToAction("Home", "Index");
            //    return View();
            return View();
        }
    }
}

