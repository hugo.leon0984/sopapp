﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Renci.SshNet;

namespace sopinfra.Models
{
    public class RedHat
    {
        public (string, string, string) Test(string id)
        //public string Statuscpuredhat(string id)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio; 
            //string host = id;
            string user = "sopapp1";
            //        string pass = "App1$sop";                

            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
    //            new PasswordAuthenticationMethod(user, pass),
            new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
            new PrivateKeyFile("../src/key/id_rsa","passphrase")
            //new PrivateKeyFile(@"C:\Users\f245277\.ssh\id_rsa","passphrase")
    }),
                     }
                );

            using var client = new SshClient(ConnNfo);
            client.Connect();

            var ps = client.RunCommand("ls /");
            string inputA = ps.Result;

            var sd = client.RunCommand("df -h");
            string inputB = sd.Result;

            var ip = client.RunCommand("cat /producto/httpd-logs/drup8dev/drup8dev_access.log | awk '{print $1}' | sort | uniq -c | sort -nr");
            string inputC = ip.Result;


            client.Disconnect();
            //(string, string) tuple(long id) // tuple return type
            // {
            //                ... // retrieve first, middle and myNum from data storage
            //    return (input, input2); // tuple literal
            // }
            return (inputA,inputB,inputC);
        }

        public string Statusdisco(string id)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            string user = "sopapp1";
            //        string pass = "App1$sop";                

            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
    //            new PasswordAuthenticationMethod(user, pass),
            new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
            new PrivateKeyFile("../src/key/id_rsa","passphrase")
            //new PrivateKeyFile(@"C:\Users\f245277\.ssh\id_rsa","passphrase")
    }),
                     }
                );

            using var client = new SshClient(ConnNfo);
            client.Connect();

            var ps = client.RunCommand("df -h");
            string input = ps.Result;
            client.Disconnect();
            return (input);
        }

        public string Ipaccess(string id)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            string user = "sopapp1";
            //        string pass = "App1$sop";                

            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
    //            new PasswordAuthenticationMethod(user, pass),
            new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
            new PrivateKeyFile("../src/key/id_rsa","passphrase")
            //new PrivateKeyFile(@"C:\Users\f245277\.ssh\id_rsa","passphrase")
    }),
                     }
                );

            using var client = new SshClient(ConnNfo);
            client.Connect();

            var ps = client.RunCommand("cat /producto/httpd-logs/drup8dev/drup8dev_access.log | awk '{print $1}' | sort | uniq -c | sort -nr");
            string input = ps.Result;
            client.Disconnect();
            return (input);
        }

    }
}





