﻿using sopinfra.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sopinfra.DBContexts
{
    public class MyDBContext : DbContext
    {
//        public DbSet<Sistema> Sistemas { get; set; }
        public DbSet<Server> Servers { get; set; }
        public DbSet<App> Apps { get; set; }
        public DbSet<Document> Documents { get; set; }

        public MyDBContext(DbContextOptions<MyDBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Use Fluent API to configure  

            // Map entities to tables  
 //           modelBuilder.Entity<Sistema>().ToTable("Sistemas");
            modelBuilder.Entity<Server>().ToTable("Servers");
            modelBuilder.Entity<App>().ToTable("Apps");
            modelBuilder.Entity<Document>().ToTable("Documents");

            // Configure Primary Keys  
            //           modelBuilder.Entity<Sistema>().HasKey(ug => ug.Id).HasName("PK_Sistemas");
            modelBuilder.Entity<Server>().HasKey(u => u.Id).HasName("PK_Server");
            modelBuilder.Entity<App>().HasKey(a => a.Id).HasName("PK_App");
            modelBuilder.Entity<Document>().HasKey(d => d.Id).HasName("PK_Document");

            // Configure indexes  
            //           modelBuilder.Entity<Sistema>().HasIndex(p => p.Name).IsUnique().HasDatabaseName("Idx_Name");
            modelBuilder.Entity<Server>().HasIndex(u => u.DnsName).HasDatabaseName("Idx_DnsName");
            modelBuilder.Entity<Server>().HasIndex(u => u.IpAddress).HasDatabaseName("Idx_IpAddress");
            modelBuilder.Entity<Server>().HasIndex(u => u.Descripcion).HasDatabaseName("Idx_Descripcion");
            modelBuilder.Entity<Server>().HasIndex(u => u.Sistema).HasDatabaseName("Idx_Sistema");

            modelBuilder.Entity<App>().HasIndex(u => u.Name).HasDatabaseName("Idx_DnsName");
            modelBuilder.Entity<App>().HasIndex(u => u.Contacto).HasDatabaseName("Idx_IpAddress");
            modelBuilder.Entity<App>().HasIndex(u => u.Descripcion).HasDatabaseName("Idx_Descripcion");
            modelBuilder.Entity<App>().HasIndex(u => u.Equipo).HasDatabaseName("Idx_Sistema");

            modelBuilder.Entity<Document>().HasIndex(u => u.Name).HasDatabaseName("Idx_DnsName");
            modelBuilder.Entity<Document>().HasIndex(u => u.Aplicacion).HasDatabaseName("Idx_IpAddress");
            modelBuilder.Entity<Document>().HasIndex(u => u.Descripcion).HasDatabaseName("Idx_Descripcion");
            modelBuilder.Entity<Document>().HasIndex(u => u.Url).HasDatabaseName("Idx_Sistema");

            // Configure columns  
            //           modelBuilder.Entity<Sistema>().Property(ug => ug.Id).HasColumnType("int").UseMySqlIdentityColumn().IsRequired();
            //           modelBuilder.Entity<Sistema>().Property(ug => ug.Name).HasColumnType("nvarchar(100)").IsRequired();
            //           modelBuilder.Entity<Sistema>().Property(ug => ug.CreationDateTime).HasColumnType("datetime").IsRequired(false);
            //           modelBuilder.Entity<Sistema>().Property(ug => ug.LastUpdateDateTime).HasColumnType("datetime").IsRequired(false);

            modelBuilder.Entity<Server>().Property(u => u.Id).HasColumnType("int").UseMySqlIdentityColumn().ValueGeneratedOnAdd().IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.DnsName).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.IpAddress).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.Descripcion).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.Sistema).HasColumnType("nvarchar(15)").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.FechaRegistro).HasColumnType("datetime").HasDefaultValueSql("CURRENT_TIMESTAMP").IsRequired();
            modelBuilder.Entity<Server>().Property(u => u.LastUpdateDateTime).HasColumnType("datetime").IsRequired(false);

            modelBuilder.Entity<App>().Property(a => a.Id).HasColumnType("int").UseMySqlIdentityColumn().ValueGeneratedOnAdd().IsRequired();
            modelBuilder.Entity<App>().Property(a => a.Name).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<App>().Property(a => a.Contacto).HasColumnType("nvarchar(100)").IsRequired();
            modelBuilder.Entity<App>().Property(a => a.Descripcion).HasColumnType("nvarchar(250)").IsRequired();
            modelBuilder.Entity<App>().Property(a => a.Equipo).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<App>().Property(a => a.FechaRegistro).HasColumnType("datetime").HasDefaultValueSql("CURRENT_TIMESTAMP").IsRequired();
            modelBuilder.Entity<App>().Property(a => a.LastUpdateDateTime).HasColumnType("datetime").IsRequired(false);

            modelBuilder.Entity<Document>().Property(d => d.Id).HasColumnType("int").UseMySqlIdentityColumn().ValueGeneratedOnAdd().IsRequired();
            modelBuilder.Entity<Document>().Property(d => d.Name).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<Document>().Property(d => d.Aplicacion).HasColumnType("nvarchar(50)").IsRequired();
            modelBuilder.Entity<Document>().Property(d => d.Descripcion).HasColumnType("nvarchar(250)").IsRequired();
            modelBuilder.Entity<Document>().Property(d => d.Url).HasColumnType("nvarchar(350)").IsRequired();
            modelBuilder.Entity<Document>().Property(d => d.FechaRegistro).HasColumnType("datetime").HasDefaultValueSql("CURRENT_TIMESTAMP").IsRequired();
            modelBuilder.Entity<Document>().Property(d => d.LastUpdateDateTime).HasColumnType("datetime").IsRequired(false);

            // Configure relationships  
            //           modelBuilder.Entity<Server>().HasOne<Sistema>().WithMany().HasPrincipalKey(ug => ug.Id).HasForeignKey(u => u.SistemaId).OnDelete(DeleteBehavior.NoAction).HasConstraintName("FK_Servers_Sistemas");
        }
    }
}