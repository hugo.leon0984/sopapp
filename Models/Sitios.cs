﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Renci.SshNet;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

namespace sopinfra.Models
{


public class Sitios
    {


        public string Runstatusweb(string id)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();
            var ps = client.RunCommand(
            "powershell ; " +
            "\"Get-website | Format-Table\";");
            // "\"Get-webapplication | Format-Table\";");
            //"\"Get-Service -Name " + servicio + "  | Format-Table -AutoSize -HideTableHeaders \";") ;
            string input = ps.Result;
            client.Disconnect();
            return input;

        }

        public string Runserversites(string id, string user, string key)
        {
            string dominio = ".corp.ute.com.uy";
            string host = id + dominio;
            //string host = id;
            //string user = "f245277";
            ConnectionInfo ConnNfo = new ConnectionInfo(host, 22, user,
            new AuthenticationMethod[]{
                    new PrivateKeyAuthenticationMethod(user,new PrivateKeyFile[]{
                    new PrivateKeyFile(key,"passphrase")
                    //new PrivateKeyFile("../src/key/id_rsa","passphrase")
                    //new PrivateKeyFile(@"D:\aqui\sopinfra\key\id_rsa","passphrase")
                        }),
                     }
                );
            using var client = new SshClient(ConnNfo);
            client.Connect();



            var ps8 = client.RunCommand(
            "powershell ; " +
            "\"Get-website | Format-Table\";");


            string resHsites = ps8.Result;



            client.Disconnect();
            return resHsites;



        }


    }
}