﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sopinfra.Models
{
    public class Server
    {
        public int Id { get; set; }
        public string DnsName { get; set; }
        public string IpAddress { get; set; }
        public string Descripcion { get; set; }
        public string Sistema { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime? LastUpdateDateTime { get; set; }
    }
}
