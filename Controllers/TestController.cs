﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace sopinfra.Controllers
{
    public class TestController : Controller
    {

        private readonly IConfiguration configuration;

        public TestController(IConfiguration iConfig)
        {
            configuration = iConfig;
        }
        [HttpGet]
        public IActionResult GetUsers()
        {
            string user = configuration.GetSection("UserConect").GetSection("usuario").Value;

            string user2 = configuration.GetValue<string>("Userconect:usuario");
            var list = new List<string>
            {
                user,
                user2
            };
            return Ok(list);
        }
    }
}
