﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using sopinfra.DBContexts;
using sopinfra.Models;


namespace sopinfra.Controllers
{
    public class DocumentController : Controller
    {
        private readonly MyDBContext myDbContext;

        public DocumentController(MyDBContext context)
        {
            myDbContext = context;
        }

        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult Index()
        {
            ViewBag.Documents = myDbContext.Documents.ToList();
            return View();
        }

        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create([Bind("Name,Aplicacion,Descripcion,Url")] Document document)
        {
            if (ModelState.IsValid)
            {
                myDbContext.Add(document);
                await myDbContext.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
                //return RedirectToAction("Index", "Home");
            }
            return View();
        }


        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Update(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var document = await myDbContext.Documents.FirstOrDefaultAsync(s => s.Id == id);
            if (await TryUpdateModelAsync<Document>(
                document,
                "",
                s => s.Name, s => s.Aplicacion, s => s.Descripcion, s => s.Url))
            {
                try
                {
                    await myDbContext.SaveChangesAsync();
                    return RedirectToAction("Index", "Document");
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
            }
            return View(document);
        }



        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult Edit(int Id)
        {
            var document = myDbContext.Documents.Find(Id);
            if (document == null)
            {
                return Redirect("Index");
            }
            return View(document);
        }


        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = await myDbContext.Documents
                .FirstOrDefaultAsync(m => m.Id == id);
            if (document == null)
            {
                return NotFound();
            }

            return View(document);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "Require.Ldap.User", AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var document = await myDbContext.Documents.FindAsync(id);
            myDbContext.Documents.Remove(document);
            await myDbContext.SaveChangesAsync();
            return RedirectToAction("Index", "Document");
        }

    }    

}

